#!/usr/bin/env python3
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import random
import re
import os
import argparse

def get_moons():
    print('\nPlease wait, collecting moons now...\n')
    options = webdriver.FirefoxOptions()
    options.add_argument('--headless')
    driver = webdriver.Firefox(options=options)
    driver.get('https://en.wikipedia.org/wiki/List_of_natural_satellites#List')
    time.sleep(2) 
    table = driver.find_element(by=By.XPATH, value='/html/body/div[3]/div[3]/div[5]/div[1]/table[4]/tbody')
    rows = table.find_elements(by=By.TAG_NAME, value='tr')
    _rows = []
    _sort_moons = []
    for row in rows:
        _rows.append(row)
    for r in _rows:
        _sort_moons.append(r.text.split()[0])
    _sort_moons.sort()
    _sort_moons = list( dict.fromkeys(_sort_moons))
    with open('moons.txt', 'w') as f:
        for moon in _sort_moons:
            f.write(moon+'\n')
    driver.close()

def main():
    parser = argparse.ArgumentParser('Force update moon list')
    parser.add_argument('--force', '-f', help='Force moon_parser to pull list of moons', action='store_true', default=False)
    args = parser.parse_args()
    if not os.path.exists('moons.txt') or args.force:
        get_moons()
    temp_moons = []
    num_of_moons = 0
    while True:
        with open('moons.txt', 'r') as f:
            num_of_moons = sum(1 for line in open('moons.txt'))
            moon_select = random.choice(f.readlines())
            if moon_select not in temp_moons and len(temp_moons) is not num_of_moons:
                print('\nMoon:', moon_select)
                temp_moons.append(moon_select)
                again = input('Find another moon name? y/n: ')
            f.close()
            if len(temp_moons) is num_of_moons:
                print('\nEnd of list\n')
                break
            if re.search('n|no', again.lower()):
                break 

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print("\nUser Quit...\n")
        exit()
